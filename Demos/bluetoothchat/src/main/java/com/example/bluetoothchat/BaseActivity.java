package com.example.bluetoothchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by OscarChen on 11/19/2015.
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected abstract int getLayoutResId();
    protected abstract String TAG();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
    }

    @Override
    protected void onStart() {
        Log.d(TAG(), "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG(), "onResume");
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.d(TAG(), "onStop");
        super.onStop();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG(), "onRestart");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG(), "onDestroy");
        super.onDestroy();
    }

}
