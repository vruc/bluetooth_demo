package com.example.bluetoothchat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by Oscar.Chen on 11/24/2015.
 */
public class BluetoothChatService {

    private static final String TAG = "BluetoothChatService";

    private static final String NAME_SECURE = "BluetoothChatSecure";
    private static final String NAME_INSECURE = "BluetoothChatInsecure";

    private static final UUID MY_UUID_SECURE = UUID.fromString("00000000-0000-0000-0000-000000000000");
    private static final UUID MY_UUID_INSECURE = UUID.fromString("11111111-1111-1111-1111-111111111111");

    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;

    // threads
    private AcceptThread mSecureAcceptThread;
    private AcceptThread mInsecureAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;

    private int mState;

    private static final int STATE_NONE = 0;       //
    private static final int STATE_LISTEN = 1;     // listening an incoming connection
    private static final int STATE_CONNECTING = 2; // init an outgoing connection
    private static final int STATE_CONNECTED = 3;  // connected to a remote device


    public BluetoothChatService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mHandler = handler;
        mState = STATE_NONE;
    }

    public synchronized void start() {
        Log.d(TAG, "start");

        // cancel the thread that completed the connection
        if(mConnectThread != null){
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // cancel any thread currently running a connection
        if(mConnectedThread != null){
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);

        // start thread to listen on a BluetoothServerSocket
        if(mSecureAcceptThread == null){
            mSecureAcceptThread = new AcceptThread(true);
            mSecureAcceptThread.start();
        }
        if(mInsecureAcceptThread == null) {
            mInsecureAcceptThread = new AcceptThread(false);
            mInsecureAcceptThread.start();
        }
    }

    public void stop() {
        Log.d(TAG, "stop");

        if(mConnectedThread != null){
            mConnectedThread.cancel();;
            mConnectedThread = null;
        }

        if(mConnectThread != null){
            mConnectThread.cancel();;
            mConnectThread = null;
        }

        if(mSecureAcceptThread!=null){
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }

        if(mInsecureAcceptThread != null){
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }


        setState(STATE_NONE);
    }

    public synchronized  void connect(BluetoothDevice device, boolean secureType){
        Log.d(TAG, "connect to: " + device);

        // cancel any thread attempting to make a connection
        if(mState == STATE_CONNECTING){
            if(mConnectThread != null){
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }
        // cancel any thread currently running a connection
        if(mConnectedThread != null){
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectThread = new ConnectThread(device, secureType);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device, final String socketType) {
        Log.d(TAG, "connected, Socket Type:" + socketType);

        // cancel the thread completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }


        mConnectedThread = new ConnectedThread(socket, socketType);
        mConnectedThread.start();

        // update UI
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    private synchronized void setState(int state){
        Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // update UI
        mHandler.obtainMessage(Constants.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();

        // obtain message more efficient than create and allocating new instance

//        Message message = new Message();
//        message.what = Constants.MESSAGE_STATE_CHANGE;
//        message.arg1 = state;
//        message.arg2 = -1;
//
//        mHandler.sendMessage(message);
    }

    public synchronized int getState(){
        return mState;
    }

    private void connectionLost(){
        // send a failure message back to the activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // start the service over to restart listening
        BluetoothChatService.this.start();
    }

    private void connectionFail(){
        // send a failure message back to the activity
        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // start the service over to restart listening
        BluetoothChatService.this.start();
    }

    /**
     * This thread runs while listening for incoming connection.
     */
    private class AcceptThread extends Thread {

        private final BluetoothServerSocket mServerSocket;
        private String mSocketType;

        public AcceptThread(boolean secure) {
            mSocketType = secure ? Constants.SECURE : Constants.INSECURE;
            BluetoothServerSocket tmp = null;

            try {
                if (secure) {
                    tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, MY_UUID_SECURE);
                } else {
                    tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE, MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
            }
            mServerSocket = tmp;
        }

        @Override
        public void run() {
            Log.d(TAG, "Socket Type: " + mSocketType + "BEGIN mAcceptThread" + this);
            setName("AcceptThread" + mSocketType);

            BluetoothSocket socket = null;

            while (mState != STATE_CONNECTED){
                try{
                    socket = mServerSocket.accept();
                } catch(IOException e){
                    Log.e(TAG, "Socket Type: " + mSocketType + " accept() failed.", e);
                    break;
                }

                // if connection was accepted
                if(socket != null){
                    synchronized (BluetoothChatService.this){
                        switch (mState){
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                connected(socket, socket.getRemoteDevice(), mSocketType);
                                break;

                            case STATE_NONE:
                            case STATE_CONNECTED:
                                try{
                                    socket.close();
                                } catch(IOException e){
                                    Log.e(TAG, "Could not close unwanted socket.", e);
                                }
                                break;
                        }
                    }
                }
            }
            Log.i(TAG, "END mAcceptThread, Socket Type:" + mSocketType);
        }

        public void cancel(){
            Log.d(TAG, "Socket Type " + mSocketType + " cancel " + this);
            try{
                mServerSocket.close();
            } catch(IOException e){
                Log.e(TAG, "Socket Type" + mSocketType + "close() of server failed", e);
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection with a device.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mSocket;
        private final BluetoothDevice mDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device, boolean secure) {
            mDevice = device;
            mSocketType = secure ? Constants.SECURE : Constants.INSECURE;
            BluetoothSocket tmp = null;
            try {
                if (secure) {
                    tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                } else {
                    tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
            }
            mSocket = tmp;
        }

        @Override
        public void run() {
            Log.i(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
            // always cancel discovery
            mAdapter.cancelDiscovery();

            try{
                mSocket.connect();
            } catch (IOException e) {
                Log.e(TAG, "unable to connect() " + mDevice.getName() + " socket type: " + mSocketType);
                // close socket
                try{
                    mSocket.close();
                } catch (IOException e1) {
                    Log.e(TAG, "unable to close() " + mSocketType + " socket during connection failure", e1);
                }
                connectionFail();
                return;
            }

            //reset the ConnectThread because we're done
            synchronized (BluetoothChatService.this){
                mConnectThread.cancel();
                mConnectThread = null;
            }

            connected(mSocket, mDevice, mSocketType);
        }

        public void cancel(){
            try{
                mSocket.close();
            } catch(IOException e){
                Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     */
    private class ConnectedThread extends Thread{

        private final BluetoothSocket mSocket;
        private final InputStream mInStream;
        private final OutputStream mOutStream;

        public ConnectedThread(BluetoothSocket socket, String socketType) {

            Log.d(TAG, "create ConnectedThread: " + socketType);

            mSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try{
                tmpIn = mSocket.getInputStream();
                tmpOut = mSocket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "create temp sockets failed", e);
            }

            mInStream = tmpIn;
            mOutStream = tmpOut;
        }

        @Override
        public void run() {
            Log.d(TAG, "BEGIN mConnectedThread" + this);

            byte[] buffer = new byte[1024];
            int bytes;

            while(true){
                try{
                    bytes= mInStream.read(buffer);

                    // update UI
                    mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer).sendToTarget();

                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                    connectionLost();
                    break;
                }
            }
         }

        public void write(byte[] buffer){
            try{
                mOutStream.write(buffer);

                // share the sent message to UI
                mHandler.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }

    }

}