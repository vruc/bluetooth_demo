package com.example.bluetoothchat;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bluetoothchat.utils.UIHelper;

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected String TAG() {
        return MainActivity.class.getSimpleName();
    }

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    private ArrayAdapter<String> mConversationArrayAdapter;

    // layout
    private ListView mConversationView;
    private Button mSendButton;
    private EditText mOutEditText;

    private StringBuffer mOutStringBuffer;

    private String mConnectedDeviceName = null;

    private BluetoothAdapter mBluetoothadapter;

    private BluetoothChatService mChatService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // init layout
        mConversationView = (ListView) findViewById(R.id.in);
        mOutEditText = (EditText) findViewById(R.id.edit_text_out);
        mSendButton = (Button) findViewById(R.id.button_send);

        // init local bluetooth adapter
        mBluetoothadapter = BluetoothAdapter.getDefaultAdapter();

        if(mBluetoothadapter == null){
            UIHelper.showTextShort(this, "Bluetooth not available");
            finish();
        }

    }

    private void setupChat(){
        Log.d(TAG(), "setup chat");

        // init conversation adapter
        mConversationArrayAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.message);
        mConversationView.setAdapter(mConversationArrayAdapter);

        mOutEditText.setOnEditorActionListener(mWriteListener);

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mOutEditText.getText().toString();
                sendMessage(message);
                mOutEditText.setText("");
            }
        });

        // init bluetooth chat service
        mChatService = new BluetoothChatService(MainActivity.this, mHandler);

        // init the buffer for outgoing message
        mOutStringBuffer = new StringBuffer("");

    }

    // make this device discoverable
    private void ensureDiscoverable(){
        if(mBluetoothadapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // If BT is not on, request that it be ebable.
        if(!mBluetoothadapter.isEnabled()){
            Log.d(TAG(), "Bluetooth Disabled");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }else {
            setupChat();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mChatService != null){
            mChatService.stop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.secure_connect:
                Intent serverIntent = new Intent(MainActivity.this,DeviceListActivity.class );
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                return true;

            case R.id.discoverable:
                return true;
        }

        return false;
    }

    private TextView.OnEditorActionListener mWriteListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            // if the action is a key-up event on the return key, send the message
            if(actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                String message = v.getText().toString();
                sendMessage(message);
            }
            return true;
        }
    };

    private void sendMessage(String message){
        Log.d(TAG(), "send message -> " + message);
    }

    private void connectDevice(Intent data, boolean secure){
        // get mac address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        Log.d(TAG(), "connect device -> " + address);

//        BluetoothDevice device = mBluetoothadapter.getRemoteDevice(address);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG(), requestCode + ", " + resultCode);

        switch (requestCode)
        {
            case REQUEST_CONNECT_DEVICE_SECURE:
                if(resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    UIHelper.showTextShort(MainActivity.this, address);
                    connectDevice(data, true);
                }
                break;
            case REQUEST_ENABLE_BT:
                if(resultCode == Activity.RESULT_OK){
                    setupChat();
                } else{
                    Log.d(TAG(), "BT not enabled");
                    UIHelper.showTextLong(MainActivity.this, getResources().getString(R.string.bt_not_enabled_leaving).toString());
                    finish();
                }
                break;
        }
    }

    private final Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

        }
    };
}
