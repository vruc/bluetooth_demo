package com.example.bluetoothchat.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Oscar.Chen on 11/24/2015.
 */
public class UIHelper {
    private UIHelper() {

    }

    public static void showTextLong(Context context, String message) {
        showText(context, message, Toast.LENGTH_LONG);
    }

    public static void showTextShort(Context context, String message) {
        showText(context, message, Toast.LENGTH_SHORT);
    }

    public static void showText(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }

}
