package com.example.beacondemo.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.estimote.sdk.Beacon;

import java.util.ArrayList;
import java.util.Collection;

import com.example.beacondemo.R;

/**
 * Created by OscarChen on 11/19/2015.
 */
public class BeaconListAdapter extends BaseAdapter {

    private ArrayList<Beacon> beacons;
    private LayoutInflater inflater;

    public BeaconListAdapter(Context context){
        this.inflater = LayoutInflater.from(context);
        this.beacons = new ArrayList<>();
    }

    public void setBeacons(Collection<Beacon> newBeacons){
        this.beacons.clear();
        this.beacons.addAll(newBeacons);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.beacons.size();
    }

    @Override
    public Beacon getItem(int position) {
        return this.beacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflateView(view, position, parent);
        bindItem(getItem(position), view);
        return view;
    }

    private void bindItem(Beacon beacon, View view){
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.macTextView.setText( String.format("MAC: %s", beacon.getMacAddress()) );
        holder.uuidTextView.setText( beacon.getProximityUUID().toString() );
        holder.majorTextView.setText("Major: " + beacon.getMajor());
        holder.minorTextView.setText("Minor: " + beacon.getMinor());
        holder.measuredPowerTextView.setText("MPower: " + beacon.getMeasuredPower());
        holder.rssiTextView.setText("RSSI: " + beacon.getRssi());

        Log.d("Beacon", beacon.getProximityUUID().toString());
    }

    private View inflateView(View view, int position, ViewGroup parent){
        if(view == null){
            view = inflater.inflate(R.layout.beacon_item, null);
            view.setTag(new ViewHolder(view));
        }
        return view;
    }

    static class ViewHolder {
        final TextView macTextView;
        final TextView uuidTextView;
        final TextView majorTextView;
        final TextView minorTextView;
        final TextView measuredPowerTextView;
        final TextView rssiTextView;

        ViewHolder(View view) {
            macTextView = (TextView) view.findViewWithTag("mac");
            uuidTextView = (TextView) view.findViewWithTag("uuid");
            majorTextView = (TextView) view.findViewWithTag("major");
            minorTextView = (TextView) view.findViewWithTag("minor");
            measuredPowerTextView = (TextView) view.findViewWithTag("mpower");
            rssiTextView = (TextView) view.findViewWithTag("rssi");
        }
    }
}
