package com.example.beacondemo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.example.beacondemo.adapters.BeaconListAdapter;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import android.bluetooth.BluetoothAdapter;

import uk.co.alt236.bluetoothlelib.device.BluetoothLeDevice;

public class BLEActivity extends BaseActivity {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_ble;
    }

    @Override
    protected String TAG() {
        return BLEActivity.class.getSimpleName();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBtLe();

//        BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
//            @Override
//            public void onLeScan(final BluetoothDevice bluetoothDevice, final int rssi, final byte[] scanData) {
//                if (scanData[7] == 0x02 && scanData[8] == 0x15) { // iBeacon indicator
//
//                    UUID uuid = getGuidFromByteArray(Arrays.copyOfRange(scanData, 9, 25));
//                    int major = (scanData[25] & 0xff) * 0x100 + (scanData[26] & 0xff);
//                    int minor = (scanData[27] & 0xff) * 0x100 + (scanData[28] & 0xff);
//                    byte txpw = scanData[29];
//
//                    Log.d("LeScanCallback", "iBeacon Packet: " + bytesToHexString(scanData));
//                    Log.d("LeScanCallback", "iBeacon Major = " + major + " | Minor = " + minor + " TxPw " + (int) txpw + " | UUID = " + uuid.toString());
//                }
//            }
//        };

    }

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothAdapter.LeScanCallback mLeScanCallback;

    /**
     * btLe scan runs all the time activity runs
     */
    private void initBtLe() {
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanData) {
                Log.d(TAG(), "onLeScan");
                if (scanData[7] == 0x02 && scanData[8] == 0x15) { // iBeacon indicator

                    UUID uuid = getGuidFromByteArray(Arrays.copyOfRange(scanData, 9, 25));
                    int major = (scanData[25] & 0xff) * 0x100 + (scanData[26] & 0xff);
                    int minor = (scanData[27] & 0xff) * 0x100 + (scanData[28] & 0xff);
                    byte txpw = scanData[29];

                    Log.d("LeScanCallback", "iBeacon Packet: " + bytesToHexString(scanData));
                    Log.d("LeScanCallback", "iBeacon Major = " + major + " | Minor = " + minor + " TxPw " + (int) txpw + " | UUID = " + uuid.toString());
                }
            }
        };

        mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    public static String bytesToHexString(byte[] bytes) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            buffer.append(String.format("%02x", bytes[i]));
        }
        return buffer.toString();
    }

    public static UUID getGuidFromByteArray(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        UUID uuid = new UUID(bb.getLong(), bb.getLong());
        return uuid;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}