package com.example.beacondemo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected String TAG() { return MainActivity.class.getSimpleName(); }

    private Button mEstimote;
    private Button mBle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mEstimote = (Button) findViewById(R.id.estimote);
        mBle = (Button) findViewById(R.id.ble);

        mEstimote.setOnClickListener(this);
        mBle.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG(), "onClick");
        switch (v.getId()){
            case R.id.estimote:
                Intent estimote = new Intent(MainActivity.this, EstimoteActivity.class);
                startActivity(estimote);
                break;

            case R.id.ble:
                Intent ble = new Intent(MainActivity.this, BLEActivity.class);
                startActivity(ble);
                break;
        }
    }
}