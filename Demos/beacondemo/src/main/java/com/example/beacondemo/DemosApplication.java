package com.example.beacondemo;

import android.app.Application;

import com.estimote.sdk.EstimoteSDK;

/**
 * Created by OscarChen on 11/19/2015.
 */
public class DemosApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //https://cloud.estimote.com/#/apps
        EstimoteSDK.initialize(this, "chanruida-outlook-com-s-yo-d4o", "bf376c1cc6bd537700bcb9ef77daf5d9");
        EstimoteSDK.enableDebugLogging(true);
    }
}
